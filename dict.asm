%include "words.inc"
global find_word
extern string_equals

section .text
; Функция принимает два аргумента: указатель на нуль-терминированную строку -> rdi и указатель на начало словаря -> rsi.
; Проходит по всему словарю в поисках подходящего ключа. Если подходящее вхождение найдено,
; вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.

find_word:
    push rdi
    push rsi 
    add rsi, 8
    call string_equals
    pop rsi
    pop rdi 
    cmp rax, 1
    je .success

    mov rsi, [rsi]
    test rsi, rsi
    jnz find_word

    xor rax, rax
    ret
    .success:
        mov rax, rsi
        ret



    
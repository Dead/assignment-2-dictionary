%define buff_size 256

%include "words.inc"
$include "lib.inc"


global _start


section .rodata

key_was_not_found_error: db "Key was not found", 10, 0
string_is_too_long_error: db "Input string is longer than 255 chars", 10, 0
enter_key_message: db "Enter key:", 10, 0

section .text

; reads string <= 255 chars in buff with stdin
; tries to find equivalation in dict
; if success prints value of that key in stdout 

    _start:
    mov rsi, buff_size
    sub rsp, rsi
    mov rdi, rsp
   
    call read_word

    ; if didn't read 
    test rax, rax
    jz .print_lenght_error

    ;now buff addr in rax and word length in rdx 
    mov rdi, rax
    mov rsi, pnt

    call find_word

    test rax, rax
    jz .print_not_found
    pop rdx
    add rax, rdx
    mov rdi, rax
    add rdi, 8
    inc rdi
    call print_string
    xor rdi, rdi
    call exit


    .print_lenght_error:
        mov rdi, string_is_too_long_error
        call .print_error

    .print_not_found:
        mov rdi, key_was_not_found_error
        call .print_error

    .print_error:
        push rdi
        call string_length
        pop rdi 
        mov rdx, rax
        mov rsi, rdi
        mov rax, 1
        mov rdi, 2
        syscall
        call exit 

    

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text

%define EXIT 60
%define SPACE_CODE 32
%define TAB_CODE 9
%define NEW_LINE_CODE 10

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
     xor rax, rax
    .count:
      cmp byte [rdi+rax], 0
      je .end 
      inc rax 
      jmp .count
    .end: 
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    push rsi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    sub rsp, 8
    mov [rsp], dil
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall
    add rsp, 8
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint: 
    mov rax, rdi
    push rcx
    mov byte [rsp], 0
    mov rsi, 1
    dec rsp
    mov rcx, 10
       .loop:
          xor rdx, rdx
          div rcx
          add rdx, '0'
          dec rsp
          mov [rsp], dl
          inc rsi
          cmp rax, 0
          jnbe .loop
    mov rdi, rsp
    push rsi
    call print_string
    pop rsi
    add rsp, rsi
    pop rcx
    ret



; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .print
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .print:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 0
    mov rcx, 0
    mov rdx, 0
    .loop:
        mov dl, [rdi + rax]
        mov cl, [rsi + rax ]
        cmp rdx, 0
        jne .not_equal
        cmp rdx, 0
        je .equal
        inc rax
        jmp .loop
    .equal:
        mov rax, 1
         ret
    .not_equal:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    dec rsp
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .end
    mov al, [rsp]
    .end:
        inc rsp
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov rax, 0
    mov rdx, 0
    cmp rsi, rdx
    je .fail
    .start:
         push rdi
        push rsi
        push rdx
        call read_char
        pop rdx
        pop rsi
        pop rdi
        cmp al, SPACE_CODE
        je .skip
        cmp al, TAB_CODE
        je .skip
        cmp al, NEW_LINE_CODE
        je .skip
        mov byte[rdi + rdx], al
        cmp al, 0
        je .success
        jmp .check
    .check:
        cmp rsi, rdx
        je .fail
        inc rdx
        jmp .start
    .skip:
        test rdx, rdx
        jz .start
        jmp .success
    .fail:
        mov rax, 0
        ret
    .success:
        mov rax, rdi
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    .loop:
        xor rsi, rsi
        mov sil, byte[rdi + rdx]
        cmp byte[rdi + rdx],'0'
        jl .end
        cmp byte[rdi + rdx], '9'
        jg .end
        push rdx
        mov rdx, 10
        mul rdx
        pop rdx
        add rax, rsi
        sub rax, '0'
        inc rdx
        jmp .loop
    .end:
        ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp rax, '-'
    je .neg
    call parse_uint
    ret
    .neg:
        inc rdi
        call parse_uint
        inc rdx
        neg rax
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    mov rax, 0
    .loop:
        cmp rax,rdx
        je .fail
        mov cl, [rdi +rax]
        mov [rsi + rax], cl
        inc rax
        cmp cl, 0
        je .success
        jmp .loop
    .fail:
         mov rax,0
         ret
    .success:
        ret

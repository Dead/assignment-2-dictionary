ASM=nasm
ASMFLAGS=-f elf64

.PHONY: clean
clean: rm *.o temp

%.o : %.asm
        nasm $(ASMFLAGS) -o $@ $<
		
lab: main.o lib.o dict.o 
		ld -o $@ $^